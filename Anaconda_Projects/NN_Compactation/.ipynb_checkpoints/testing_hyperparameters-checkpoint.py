import numpy as np
import pandas as pd

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler

##################################################################################################

# number of subprocesses to use for data loading
num_workers = 4
# how many samples per batch to load
batch_size = 60
# percentage of training set to use as validation
valid_size = 0.1

# convert data to torch.FloatTensor
transform = transforms.ToTensor()

# choose the training and test datasets
train_data = datasets.MNIST(root='data', train=True,
                                   download=False, transform=transform)
test_data = datasets.MNIST(root='data', train=False,
                                  download=False, transform=transform)

############################################################################################

# define the NN architecture

class NN(nn.Module):
    def __init__(self, functions):
        super(NN, self).__init__()
        input_ = 28 * 28
        hidden_1 = 300
        hidden_2 = 100
        output = 10
        self.fc1 = nn.Linear(input_, hidden_1)
        self.fc2 = nn.Linear(hidden_1, hidden_2)
        self.fc3 = nn.Linear(hidden_2, output)
        # {leaky_relu : 0, sigmoid : 1, softmax : 2}
        self.functions = functions

    def forward(self, x):
        # flatten image input
        x = x.view(-1, 28 * 28)
        # add hidden layer, with relu activation function
        if (self.functions[0] == 0):
            x = F.leaky_relu(self.fc1(x))
        elif (self.functions[0] == 1):
            x = F.sigmoid(self.fc1(x))
        else :
            x = F.softmax(self.fc1(x))
        # add hidden layer, with relu activation function
        if (self.functions[1] == 0):
            x = F.leaky_relu(self.fc2(x))
        elif (self.functions[1] == 1):
            x = F.sigmoid(self.fc2(x))
        else :
            x = F.softmax(self.fc2(x))
        # add output layer
        if (self.functions[2] == 0):
            x = F.leaky_relu(self.fc3(x))
        elif (self.functions[2] == 1):
            x = F.sigmoid(self.fc3(x))
        else :
            x = F.softmax(self.fc3(x))
        return x
    
#########################################################################################################

def get_criterion(id):
    if (id == 0):
        return nn.NLLLoss()
    elif (id == 1):
        return nn.CrossEntropyLoss()

table = []
    
for func_1 in range(0, 3):
    #for func_2 in range(0, 3):
        #for func_3 in range(0, 3):
            #for crit in range(0, 2):
    for weight in (0.0, 0.8, 1.6):
        print("\tI'm here with the combination {}-{}-{}-{}".format(func_1, func_1, func_1, weight))
        # obtain training indices that will be used for validation
        num_train = len(train_data)
        indices = list(range(num_train))
        np.random.shuffle(indices)
        split = int(np.floor(valid_size * num_train))
        train_idx, valid_idx = indices[split:], indices[:split]
        # define samplers for obtaining training and validation batches
        train_sampler = SubsetRandomSampler(train_idx)
        valid_sampler = SubsetRandomSampler(valid_idx)
        # prepare data loaders
        train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size,
            sampler=train_sampler, num_workers=num_workers)
        valid_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, 
            sampler=valid_sampler, num_workers=num_workers)
        test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, 
            num_workers=num_workers)

        line = [func_1, func_1, func_1, weight]
        history_loss = []

        functions = [func_1, func_1, func_1]
        model = NN(functions)
        # specify loss function (categorical cross-entropy)
        #criterion = get_criterion(crit)
        criterion = nn.CrossEntropyLoss()
        # specify optimizer (stochastic gradient descent) and learning rate = 0.01
        optimizer = torch.optim.Adam(model.parameters(), lr=0.0012, weight_decay=weight)
        # number of epochs to train the model
        n_epochs = 30
        # initialize tracker for minimum validation loss
        valid_loss_min = np.Inf # set initial "min" to infinity

        for epoch in range(n_epochs):
            print("\t\tepoch {}".format(epoch))
            # monitor training loss
            train_loss = 0.0
            valid_loss = 0.0

            ###################
            # train the model #
            ###################
            model.train() # prep model for training
            for data, target in train_loader:
                # clear the gradients of all optimized variables
                optimizer.zero_grad()
                # forward pass: compute predicted outputs by passing inputs to the model
                output = model(data)
                values, indices = torch.max(output, 0)
                # calculate the loss
                loss = criterion(output, target)
                # backward pass: compute gradient of the loss with respect to model parameters
                loss.backward()
                # perform a single optimization step (parameter update)
                optimizer.step()
                # update running training loss
                train_loss += loss.item()*data.size(0)

            ######################    
            # validate the model #
            ######################
            model.eval() # prep model for evaluation
            for data, target in valid_loader:
                # forward pass: compute predicted outputs by passing inputs to the model
                output = model(data)
                # calculate the loss
                loss = criterion(output, target)
                # update running validation loss 
                valid_loss += loss.item()*data.size(0)

            # print training/validation statistics 
            # calculate average loss over an epoch
            train_loss = train_loss/len(train_loader.sampler)
            valid_loss = valid_loss/len(valid_loader.sampler)

            history_loss.append([train_loss, valid_loss])

        # the train ends here
        history_dataframe = pd.DataFrame(history_loss, columns= ['train_loss', 'valid_loss'])
        print("\t\tsaving history_dataframe.csv")
        history_dataframe.to_csv("history_loss_{}_{}_{}_{}.csv".format(func_1, func_1, func_1, weight), index=False)

        # Now evaluating in the test 
        test_loss = 0.0
        class_correct = list(0. for i in range(10))
        class_total = list(0. for i in range(10))

        model.eval() # prep model for evaluation

        for data, target in test_loader:
            # forward pass: compute predicted outputs by passing inputs to the model
            output = model(data)
            # calculate the loss
            loss = criterion(output, target)
            # update test loss 
            test_loss += loss.item()*data.size(0)
            # convert output probabilities to predicted class
            _, pred = torch.max(output, 1)
            # compare predictions to true label
            correct = np.squeeze(pred.eq(target.data.view_as(pred)))
            # calculate test accuracy for each object class
            for i in range(len(target)):
                label = target.data[i]
                class_correct[label] += correct[i].item()
                class_total[label] += 1
        test_loss = test_loss/len(test_loader.sampler)
        line.append(test_loss)
        for x in (100. * np.sum(class_correct) / np.sum(class_total), np.sum(class_correct), np.sum(class_total)):
            line.append(x)

        # ends the most inner for
        table.append(line)
        print("More one line appended.")

table_history = pd.DataFrame(table, columns = ['fc1', 'fc2', 'fc3', 'weight', 'loss', '0_acc', '1_acc', '2_acc', '3_acc', '4_acc', '5_acc', '6_acc', '7_acc', '8_acc', '9_acc'])
table_history.to_csv("final_results.csv", index=False)
