import numpy as np
from random import shuffle


def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    for i in range(X.shape[0]):
        scores = X[i].dot(W)
        divident = np.exp(scores[y[i]])
        divisor = np.sum(np.exp(scores))
        loss += -np.log(divident/divisor)
        
        dW[:, y[i]] += (-1) * (divisor - divident) / divisor * X[i]
        for j in range(W.shape[1]):
            if j == y[i]:
                pass
            dW[:,j] += np.exp(scores[j]) / divisor * X[i]
    
    
    loss /= X.shape[0]
    loss += reg * np.sum(W * W)
    
    dW /= X.shape[0]
    dW += 2 * reg * W
    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    score = X.dot(W)
    score -= score.max()
    score = np.exp(score)
    
    score_sum = np.sum(score, axis=1)
    cors = score[range(X.shape[0]), y]
    loss = -np.sum(np.log(cors / score_sum)) / X.shape[0] + reg * np.sum(W * W)   

    s = np.divide(score, score_sum.reshape(X.shape[0], 1))
    s[range(X.shape[0]), y] = - (score_sum - cors) / score_sum
    dW = X.T.dot(s)
    dW /= X.shape[0]
    dW += 2 * reg * W
    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################

    return loss, dW
