import numpy as np
from random import shuffle

def svm_loss_naive(W, X, y, reg, test=False):
    """
    Structured SVM loss function, naive implementation (with loops).

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    dW = np.zeros(W.shape) # initialize the gradient as zero
     
    
    # compute the loss and the gradient
    num_classes = W.shape[1]
    num_train = X.shape[0]
    loss = 0.0
    for i in xrange(num_train):
        scores = X[i].dot(W)
        correct_class_score = scores[y[i]]
        for j in xrange(num_classes):
            if j == y[i]:
                continue
            margin = scores[j] - correct_class_score + 1 # note delta = 1
            if margin > 0:
                loss += margin
                dW[:,y[i]]-= X[i,:].T
                dW[:,j] += X[i,:].T

    # Right now the loss is a sum over all training examples, but we want it
    # to be an average instead so we divide by num_train.
    loss /= num_train  
    
    # Divide the gradient by the number of training examples
    dW /= num_train
    
    # Regularization
    dW += reg*W
    loss += 0.5 * reg * np.sum(W * W)
    
    
    return loss, dW


def svm_loss_vectorized(W, X, y, reg):
    """
    Structured SVM loss function, vectorized implementation.

    Inputs and outputs are the same as svm_loss_naive.
    """
    loss = 0.0
    dW = np.zeros(W.shape)
    
    scores = X.dot(W)
    margins = np.maximum(0, scores - scores[range(X.shape[0]),y].reshape(-1,1) + 1)
    margins[range(X.shape[0]),y] = 0
    
    loss = np.sum(margins)
    loss /=  X.shape[0]
    loss += 0.5 * reg * np.sum(W * W)


    #############################################################################
    # TODO:                                                                     #
    # Implement a vectorized version of the gradient for the structured SVM     #
    # loss, storing the result in dW.                                           #
    #                                                                           #
    # Hint: Instead of computing the gradient from scratch, it may be easier    #
    # to reuse some of the intermediate values that you used to compute the     #
    # loss.                                                                     #
    #############################################################################
    
    margins[margins > 0] = 1
    margins[range(X.shape[0]),y] = -np.sum(margins,axis=1)
    dW = X.T.dot(margins)
    
    dW /= X.shape[0]
    dW += reg * W
    
    
    #############################################################################
    #                             END OF YOUR CODE                              #
    #############################################################################

    return loss, dW
